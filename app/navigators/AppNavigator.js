/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import Login from '../pages/init/Login';
import Register from '../pages/init/Register';
import CoreAppNavigator from './CoreAppNavigator';
import AddEvent from '../pages/events/EventForm';
import ProfileForm from '../pages/profile/ProfileForm';

export default StackNavigator({
  login: { screen: Login },
  register: { screen: Register },
  coreApp: { screen: CoreAppNavigator },
  profileForm: {screen: ProfileForm},
  addEvent: { screen: AddEvent },
}, {
  headerMode: 'screen'
});