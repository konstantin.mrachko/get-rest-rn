/**
 * Created by kostiantynmrachko on 5/23/17.
 */
import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import EventsMap from '../pages/events/EventsMap';
import EventsList from '../pages/events/EventsList';
import EventsHeader from '../components/events/EventsHeader';

const EventsTabNavigator = TabNavigator({
  eventsMap: { screen: EventsMap },
  eventsList: { screen: EventsList },
});

EventsTabNavigator.navigationOptions = ({navigation}) => {
  const title = 'События';
  return {
    title,
    header: <EventsHeader title={title} showDrawer={() => navigation.navigate('DrawerOpen')} onSearchChange={value => console.log(value)}/>
  }
};

export default StackNavigator({
  eventsStack: { screen: EventsTabNavigator }
});