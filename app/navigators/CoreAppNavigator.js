/**
 * Created by kostiantynmrachko on 5/24/17.
 */
import React from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import EventsMap from '../pages/events/EventsMap';

export default CoreAppNavigator = DrawerNavigator({
  events: { screen: StackNavigator({eventsStack: {screen: EventsMap}}) }
}, {
  drawerWidth: 200,
});

CoreAppNavigator.navigationOptions = {
  header: null
};