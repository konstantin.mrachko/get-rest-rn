/**
 * Created by kostiantynmrachko on 7/13/17.
 */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconButton from '../general/IconButton';
import styles from '../../styles/components/eventsHeader';

export default class EventsHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearchActive: false,
      searchString: ''
    };
    this.onSearchChange = this.onSearchChange.bind(this); //todo add debounce
  }
  onSearchChange(value) {
    this.setState({searchString: value});
    this.props.onSearchChange(value);
  }
  render() { //todo add sorting dropdown
    return (
      <View
        style={styles.wrapper}>
        { this.state.isSearchActive ?
          <View
            style={[styles.container, {flex: 1}]}>
            <IconButton
              iconName="arrow-back"
              onPress={() => this.setState({isSearchActive: false, searchString: ''})}/>
            <TextInput
              autoFocus
              selectionColor="black"
              underlineColorAndroid="black"
              style={styles.searchInput}
              value={this.state.searchString}
              onChangeText={this.onSearchChange}/>
          </View> :
          <View
            style={styles.container}>
            <IconButton
              iconName="menu"
              onPress={this.props.showDrawer}/>
            <Text
              style={styles.title}>{this.props.title}</Text>
          </View>
        }
        <View
          style={styles.container}>
          <IconButton
            iconName="search"
            rightMargin
            onPress={() => this.setState({isSearchActive: true})}/>
          <IconButton
            iconName="sort"
            onPress={this.props.showDrawer}/>
        </View>
      </View>
    );
  }
}