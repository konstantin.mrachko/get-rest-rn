import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TouchableWithoutFeedback, Image, Picker } from 'react-native';
import styles from '../../../styles/components/event';
import SwitchableButton from '../../general/SwitchableButton';
import { utils } from '../../../styles/global';

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      //language: 'js'
    }
  }
  onPress() {
    this.props.navigation.navigate('event');
  }
  render() {
    return (
      <View>
        <TouchableOpacity onPress={()=>this.onPress()}>
          <Image source={{ uri: this.props.image }} style={styles.wrapper}>
            <View style={[utils.flexDirectionRow, styles.textTop]}>
              <Text style={styles.text}>{`${this.props.participants} учасников, ${this.props.friends} из них друзей`}</Text>
            </View>
            <View>
              <Text style={[styles.text]}>{this.props.date}</Text>
              <Text style={[styles.textBig]}>{this.props.name}</Text>
              <Text style={[styles.textSmall]}>{this.props.place}</Text>
            </View>
          </Image>
        </TouchableOpacity>
        <View style={ styles.buttonsWrapper}>
          <SwitchableButton />
        </View>
      </View>
    );
  }
}

// <Picker
// selectedValue={this.state.language}
// onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
// <Picker.Item label="Java" value="java" />
// <Picker.Item label="JavaScript" value="js" />
//   </Picker>