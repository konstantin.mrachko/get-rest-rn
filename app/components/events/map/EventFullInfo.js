import React, { Component } from 'react';
import { ScrollView, View, Image, Text, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { utils } from '../../../styles/global';
import styles from '../../../styles/pages/events/event';
import SwitchableButton from '../../general/SwitchableButton';
import Button from '../../general/Button';
import moment from 'moment';
import setUserEventRelation from '../../../actions/events/setUserEventRelation';

const icon = 'https://cdn3.iconfinder.com/data/icons/auto-racing/423/Stopwatch_Timer-512.png';

class EventFullInfo extends Component {
  static navigationOptions = {
    title: 'Мероприятие'
  };
  onPress() {
    this.props.navigation.navigate('message');
  }
  setRelation(visiting) {
    const id = this.props.event.idEvent;
    console.log(id,visiting)
    this.props.setUserEventRelation(id,visiting);
  }
  render() {
    const {event} = this.props;
    moment.locale('ru');
    let visiting = false;
    if (this.props.loaded) {
      visiting = !!this.props.event.participants
        .filter(user => user.id === this.props.userId).length
    }
    return(
      <ScrollView style={[styles.wrapper, this.props.style]}>
        {this.props.loaded &&
        <View>
          <View style={styles.header}>
            <View style={styles.dateWrapper}>
              <Text style={styles.textDate}>
                {moment(event.startTime).format('D')}
              </Text>
              <Text style={styles.textSmall}>
                {moment(event.startTime).format('MMMM')}
              </Text>
            </View>
            <View style={styles.descriptionWrapper}>
              <View style={utils.flexDirectionRow}>
                <Text style={styles.textSmall}>
                  {'Организатор: '}
                </Text>
                <Text style={[styles.textSmall, utils.textBold]}>
                  {this.props.event.author.name}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.buttonsWrapper}>
            <SwitchableButton pressed={visiting} action={visiting => this.setRelation(visiting)} />
            <Button text="Чат" filled buttonStyle={utils.regularButton}
                    textStyle={utils.regularButtonText} onPress={()=>this.onPress()}/>
          </View>
          <View style={styles.contentWrapper}>
            <View style={[utils.flexDirectionRow, utils.marginBottom20]}>
              <Image source={{ uri: icon }} style={styles.icon} />
              <Text style={[styles.text, utils.flex1]}>
                {moment(event.startTime).calendar()}
              </Text>
            </View>
            <View style={[utils.flexDirectionRow, utils.marginBottom20]}>
              <Image source={{ uri: icon }} style={styles.icon} />
              <Text style={[styles.text, utils.flex1]}>
                {`${event.participants.length} идут`}
              </Text>
              <View>
              </View>
            </View>
            <View style={[utils.flexDirectionRow, utils.marginBottom20]}>
              <Image source={{ uri: icon }} style={styles.icon} />
              <Text style={[styles.text, utils.flex1]}>
                {event.address}
              </Text>
            </View>
            <View>
              <Text style={[styles.text, utils.textBold]}>
                {'Описание'}
              </Text>
              <Text style={styles.text}>
                {event.fullDesc}
              </Text>
            </View>
          </View>
        </View>}
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    userId: state.authentication.userId,
  }
}

export default connect(null, { setUserEventRelation })(EventFullInfo);