/**
 * Created by kostiantynmrachko on 6/6/17.
 */
import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {Svg, Path, Defs, LinearGradient, Stop} from 'react-native-svg';
import styles from '../../../styles/components/events/map/eventMarker';
import { colors } from '../../../constants/uiConstants';
import moment from 'moment';

export default class EventMarker extends Component {
  shadeColor(color, percent) {
    if (percent > 1) percent = 1;
    let f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
  }
  render() {
    const {event} = this.props;
    // let opacity;
    // let daysLeft = moment(event.date).diff(moment(), 'days');
    // if (daysLeft > 7) {
    //   opacity = 0.3;
    // } else {
    //   opacity = 1 - daysLeft / 10;
    // }
    return (
      <View style={styles.markerWrapper}>
        <Svg width="44" height="54">
          <Defs>
            <LinearGradient id="grad" x1="0" y1="0" x2="0" y2="52">
              <Stop offset="0.3" stopColor={colors.marker}/>
              <Stop offset="1.3" stopColor="white"/>
            </LinearGradient>
          </Defs>
          <Path
            d="M 2 22 A 12 12 0 1 1 42 22 C 42 32 32 42 22 52 C 12 42 2 32 2 22 Z"
            stroke={this.props.selected ? colors.marker : 'white'}
            strokeWidth="3"
            fill="url(#grad)"
          />
        </Svg>
        <Text style={styles.markerText}>{event.participants.length.toString()}</Text>
      </View>
    );
  }
}