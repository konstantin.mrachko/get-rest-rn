/**
 * Created by kostiantynmrachko on 6/27/17.
 */
import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import TextButton from '../../general/TextButton';
import styles from '../../../styles/components/events/map/eventInfo';

export default class EventInfo extends Component {
  render() {
    const {event} = this.props;
    return (
      <View style={[styles.wrapper, this.props.style]}>
        {this.props.loaded &&
        <View>
          <View>
            <Text>Date</Text>
          </View>
          <View style={styles.textWrapper}>
            <Text style={styles.title}>
              {event.eventName}
            </Text>
            <Text style={styles.participantsCount}>
              {event.participants.length} участника
            </Text>
            <Text style={styles.authorName}>
              {event.author.name}
            </Text>
          </View>
        </View>}
      </View>
    );
  }
}