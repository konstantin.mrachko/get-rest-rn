/**
 * Created by kostiantynmrachko on 6/29/17.
 */
import React, { Component } from 'react';
import { View, Text } from 'react-native'
import styles from '../../../styles/components/events/map/eventMarker';

export default class UserMarker extends Component {
  render() {
    return (
      <View style={styles.markerWrapper}>
        <Text style={styles.markerText}>
          U
        </Text>
      </View>
    );
  }
}