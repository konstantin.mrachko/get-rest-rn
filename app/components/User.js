import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import styles from '../styles/components/user';
import { utils } from '../styles/global';

class User extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return(
      <TouchableOpacity style={styles.wrapper}>
        <Image source={{ uri: this.props.img }} style={styles.img} />
        <Text style={styles.text} >
          {this.props.name}
        </Text>
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(User);