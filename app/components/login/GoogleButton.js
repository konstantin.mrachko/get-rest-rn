/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import React, {Component} from 'react';
import styles from '../../styles/components/login/googleButton';
import Button from '../general/Button';

export default class GoogleButton extends Component {
  render() {
    return (
      <Button
        text="Войти с Google"
        buttonStyle={[styles.wrapper, this.props.buttonStyle]}
        filled
        onPress={this.props.onPress}
      />
    );
  }
}