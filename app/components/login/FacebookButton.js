/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import React, {Component} from 'react';
import styles from '../../styles/components/login/facebookButton';
import Button from '../general/Button';

export default class FacebookButton extends Component {
  render() {
    return (
      <Button
        text="Войти с Facebook"
        buttonStyle={[styles.wrapper, this.props.buttonStyle]}
        filled
        onPress={this.props.onPress}
      />
    );
  }
}