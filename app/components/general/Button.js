/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from '../../styles/components/button';

export default class Button extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[ this.props.filled ? styles.filledWrapper: styles.wrapper,
          this.props.buttonStyle]}
        onPress={this.props.onPress}>
        <Text style={[this.props.filled ? styles.filledText: styles.text,
          this.props.textStyle]}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}