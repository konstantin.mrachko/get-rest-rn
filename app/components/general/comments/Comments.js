import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, Image, TextInput } from 'react-native';
import styles from '../../styles/components/comments/comments';
import { utils } from '../../styles/global';
import { colors } from '../../constants/uiConstants';
import Comment from './Comment';

const image = 'https://velomesto.ru/media/new_uploads/kask_protone_helmet_large.jpg';
const text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis';
const name = 'Иван Петренко';
const date = '8 сентября, 22:45';

const comments = [
  { image, text, name, date},
  { image, text, name, date},
  { image, text, name, date},
  { image, text, name, date},
  { image, text, name, date},
];

export default class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      writing: false,
    };
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <Text style={styles.title}>
          {'Комментарии'}
        </Text>
        <View style={styles.inputWrapper}>
          <View style={utils.flexDirectionRow}>
            <Image source={{ uri: image }} style={styles.image} />
            <TextInput
              onFocus={()=>this.setState({ writing: !this.state.writing })}
              style={[this.state.writing && styles.writing, styles.input]}
              onChangeText={(comment) => this.setState({comment})}
              value={this.state.comment}
              placeholder={!this.state.writing ? "Добавить комментарий" : ''}
              underlineColorAndroid='rgba(0,0,0,0)'
              selectionColor={colors.accent}
              multiline
            />
          </View>
          {this.state.writing &&
          <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('red')}
            onPress={()=>this.setState({ comment: '' })}>
            <Text style={styles.sendButton}>
              {'Отправить'}
            </Text>
          </TouchableNativeFeedback>}
        </View>
        {comments.map((item, index) =>
          <Comment image={item.image} text={item.text} name={item.name} date={item.date} key={index} />
        )}
      </View>
    );
  }
}