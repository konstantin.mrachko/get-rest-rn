import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import styles from '../../styles/components/comments/comment';
import { utils } from '../../styles/global';
import { colors } from '../../constants/uiConstants';

export default class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
    };
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.titleRow}>
          <Image source={{ uri: this.props.image }} style={styles.image} />
          <View style={utils.flex1}>
            <Text style={styles.name}>
              {this.props.name}
            </Text>
            <Text style={styles.date}>
              {this.props.date}
            </Text>
          </View>
        </View>
        <Text style={styles.text}>
          {this.props.text}
        </Text>
      </View>
    );
  }
}