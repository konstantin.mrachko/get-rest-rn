/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import React, {Component} from 'react';
import styles from '../../styles/components/textButton';
import Button from './Button';

export default class TextButton extends Component {
  render() {
    return (
      <Button
        text={this.props.text}
        buttonStyle={[styles.wrapper, this.props.buttonStyle]}
        textStyle={this.props.textStyle}
        onPress={this.props.onPress}
      />
    );
  }
}