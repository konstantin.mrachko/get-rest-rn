import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from '../../styles/components/switchableButton';

export default class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pressed: this.props.pressed,
    };
  }
  onPress() {
    this.props.action(!this.state.interesting);
    this.setState({ interesting: !this.state.interesting });
  }
  render() {
    return (
      <TouchableOpacity onPress={() => this.onPress()}>
        <View style={[this.state.interesting && styles.pressedButton, styles.button]}>
          <Text style={[this.state.interesting && styles.pressedButtonText, styles.textButton]}>{'Пойду'}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}