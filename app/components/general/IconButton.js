/**
 * Created by kostiantynmrachko on 5/24/17.
 */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from '../../styles/components/iconButton';

export default class IconButton extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[
          styles.wrapper, {
            marginRight: this.props.rightMargin ? 16 : 0,
            marginLeft: this.props.leftMargin ? 16 : 0
          }]}
        onPress={this.props.onPress}>
        <Icon
          name={this.props.iconName}
          color={this.props.color || "black"}
          size={24}/>
      </TouchableOpacity>
    );
  }
}