/**
 * Created by kostiantynmrachko on 8/6/17.
 */
import React, { Component } from 'react';
import { View, Text, Image, TextInput, DatePickerAndroid, TouchableNativeFeedback } from 'react-native';
import { connect } from 'react-redux';
import * as ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import styles from '../../styles/pages/profile/profileForm';
import { utils } from '../../styles/global';
import Button from '../../components/general/Button';

class ProfileForm extends Component {
  static navigationOptions = {
    title: 'Ваш профиль'
  };
  constructor(props) {
    super(props);
    this.state = {
      name: props.name ? props.name : '',
      birthdate: props.birthdate ? props.birthdate : moment().unix(),
      image: props.image ? {uri: props.image} : null,
      imagePicked: !!props.image
    };
    this.showDatePicker = this.showDatePicker.bind(this);
    this.showImagePicker = this.showImagePicker.bind(this);
  }
  showDatePicker() {
    const options = {
      date: moment.unix(this.state.birthdate).valueOf(),
      maxDate: new Date()
    };
    DatePickerAndroid.open(options)
      .then(({action, year, month, day}) => {
        if (action !== DatePickerAndroid.dismissedAction) {
          this.setState({birthdate: moment({year, month, day}).unix()});
        }
      });
  }
  showImagePicker() {
    ImagePicker.showImagePicker({
      title: 'Ваше фото',
      cancelButtonTitle: 'Отмена',
      takePhotoButtonTitle: 'Сделать фото',
      chooseFromLibraryButtonTitle: 'Выбрать из галереи',
      mediaType: 'photo',
      permissionDenied: {
        title: 'Нет доступа к фото',
        text: 'Чтобы загрузить фото, предоставьте доступ к съемке и галерее',
        reTryTitle: 'Снова',
        okTitle: 'Я уверен'
      }
    }, response => {
      if (!(response.didCancel || response.error || response.customButton)) {
        const {uri, type, fileName} = response;
        this.setState({image: {uri, type, name: fileName}, imagePicked: true});
      }
    });
  }
  onPress() {
    this.props.navigation.navigate('coreApp');
  }
  render() {
    console.log(this.state);
    return(
      <View style={styles.wrapper}>
        <View style={styles.loginWrapper}>
          <TextInput
            style={styles.registerInput}
            onChangeText={name => this.setState({name})}
            value={this.state.name}
            placeholder="Ваше имя"
            underlineColorAndroid="transparent"
          />
          <Text>Дата рождения</Text>
          <TouchableNativeFeedback
            onPress={this.showDatePicker}>
            <View>
              <Text>{moment.unix(this.state.birthdate).format('DD.MM.YYYY')}</Text>
            </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback
            onPress={this.showImagePicker}>
            <View>
              {this.state.imagePicked ?
                <Image style={{width: 200, height: 200}} source={this.state.image}/>:
                <Text>Выберите свою фотографию</Text>
              }
            </View>
          </TouchableNativeFeedback>
          <Button text="Вперед!" filled buttonStyle={styles.registerButton} onPress={()=>this.onPress()}/>
        </View>
      </View>
    );
  }
}

export default ProfileForm;