import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import User from '../../components/User';

class UsersList extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return(
      <ScrollView>
        {
          this.props.users.map((item, index) =>
            <User key={index} name={item.name} img={item.img} />
          )
        }
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: [
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
      { name: 'Вася Вася', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE' },
    ]
  };
}

export default connect(mapStateToProps)(UsersList);