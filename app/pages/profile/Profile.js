import React, { Component } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import MapView from 'react-native-maps';
import moment from 'moment';
import styles from '../../styles/pages/profile/profile';
import { utils } from '../../styles/global';
import Button from '../../components/general/Button';
import EventMarker from '../../components/events/map/EventMarker';

const userImage = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkS9mYMsnJzmMlrRWyLN_-2GHOLaD90A66nz2rcICgy0zmgbUE';

class Profile extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      name: 'Александр Владимирович',
      birthdate: moment().diff(moment(new Date('06/07/1994')), 'years'),
      followersCount: 12,
      subscribersCount: 4,

    };
  }
  render() {
    return(
      <ScrollView style={utils.backgroundColorWhite}>
        <Image source={{ uri: userImage }} style={styles.userImage} />
        <Text style={styles.name}>
          {this.state.name + ', ' + this.state.birthdate}
        </Text>
        <View style={utils.flexDirectionRow}>
          <Button onPress={() => {}} style={styles.placeButton} text={'Подписаться'} />
          <Button onPress={() => this.props.navigation.navigate('message')} style={styles.placeButton} text={'Чат'} />
        </View>
        <View style={[utils.flexDirectionRow, utils.alignSelfStretch]}>
          <TouchableOpacity style={styles.followersWrapper}>
            <Text style={styles.followersText}>
              {'Подписчики'}
            </Text>
            <Text style={styles.followersCount}>
              {this.state.followersCount + ''}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.followersWrapper}>
            <Text style={styles.followersText}>
              {'Подписки'}
            </Text>
            <Text style={styles.followersCount}>
              {this.state.followersCount + ''}
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity >
          <Text style={styles.events}>
            {'Мероприятия'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.mapWrapper}>
          <MapView
            style={styles.map}
            initialRegion={{
              latitude: 47.910444,
              longitude: 33.391776,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            {this.props.events.map(event => {
              return (
                <MapView.Marker
                  key={event.id}
                  style={{zIndex: event.participantsCount}}
                  coordinate={{
                    latitude: event.lat,
                    longitude: event.long,
                  }}>
                  <EventMarker
                    selected={(this.state.selectedEventId === event.id) && this.state.showEventInfo}
                    event={event}/>
                </MapView.Marker>);
            })}
          </MapView>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {events: []};
}

export default connect(mapStateToProps)(Profile);