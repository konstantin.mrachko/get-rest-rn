/**
 * Created by kostiantynmrachko on 5/23/17.
 */
import React, {Component} from 'react';
import {
  View,
  Text,
  Alert,
  Animated,
  PanResponder,
  AsyncStorage,
  Dimensions,
} from 'react-native'
import {connect} from 'react-redux';
import MapView from 'react-native-maps';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import EventMarker from '../../components/events/map/EventMarker';
import UserMarker from '../../components/events/map/UserMarker';
import EventInfo from '../../components/events/map/EventInfo';
import EventFullInfo from '../../components/events/map/EventFullInfo';
import styles from '../../styles/pages/events/eventsMap';
import addEvent from '../../actions/events/createEvent';
import {utils} from '../../styles/global';
import Button from '../../components/general/Button';
import EventsHeader from '../../components/events/EventsHeader';
import getEvents from '../../actions/events/getEvents';

const PANEL_HEIGHT = 100;
const FULL_INFO_HEIGHT = Dimensions.get('window').height * 0.5;

class EventsMap extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: 'События',
      header: <EventsHeader
        title={'События'}
        showDrawer={() => navigation.navigate('DrawerOpen')}
        onSearchChange={value => console.log(value)}/>
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: null,
      selectedEventId: null,
      pan: new Animated.Value(PANEL_HEIGHT),
      showEventInfo: ''
    };
    this.locationWatcher = null;
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (e, gestureState) => {
        this.state.pan.setOffset(this.state.pan._value);
        this.state.pan.setValue(0);
      },
      onPanResponderMove: Animated.event([
        null, {dy: this.state.pan},
      ]),
      onPanResponderRelease: (e, {dy}) => {
        this.state.pan.flattenOffset();
        if (dy > 0) {
          if (this.state.showEventInfo === 'full') {
            Animated.spring(this.state.pan, {
              toValue: 0
            }).start(() => this.setState({showEventInfo: 'visible'}));
          } else {
            this.setState({showEventInfo: ''});
            Animated.spring(this.state.pan, {
              toValue: PANEL_HEIGHT
            }).start();
          }
        } else {
          Animated.spring(this.state.pan, {
            toValue: -1 * FULL_INFO_HEIGHT
          }).start(() => this.setState({showEventInfo: 'full'}));
        }
      }
    });
    this.inputRange = [];
    this.outputRange = [];
    for (let i = -10 * PANEL_HEIGHT; i <= 2 * PANEL_HEIGHT; i+= 2 * PANEL_HEIGHT) {
      this.inputRange.push(i);
      this.outputRange.push(i >= -1 * PANEL_HEIGHT ? 0 : (i-PANEL_HEIGHT/2)/2);
    }
  }
  onLocationUpdated(location) {
    this.setState({currentLocation: location.coords})
  }
  onLocationError(error) {
    console.log(error);
  }
  setLocationWatcher() {
    navigator.geolocation.clearWatch(this.locationWatcher);
    this.locationWatcher = navigator.geolocation.watchPosition(
      this.onLocationUpdated.bind(this),
      this.onLocationError.bind(this),
      {
        enableHighAccuracy: false,
        maximumAge: 1000,
        distanceFilter: 10,
        timeout: 10000
      });
  }
  componentDidMount() {
    this.props.getEvents();

    AsyncStorage.getItem('show-geo-dialog')
      .then(showDialog => {
        if (showDialog === 'true') {
          AsyncStorage.setItem('show-geo-dialog', 'false');
          return LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h3>Доступ к местоположению</h3> Разрешите использовать геолокацию для поиска ближайших мероприятий?",
            ok: "Разрешить",
            cancel: "Нет"
          });
        }
      }).then(() => {
        this.setLocationWatcher();
      }).catch(this.onLocationError.bind(this));
  }
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.locationWatcher);
  }
  showEventInfo(value) {
    if (value === !this.state.showEventInfo) {
      console.log(value, this.state.showEventInfo);
      Animated.timing(this.state.pan, {
        toValue: value ? 0 : PANEL_HEIGHT,
        duration: 500
      }).start();
      this.setState({showEventInfo: value ? 'visible' : ''});
    }
  }
  render() {
    const {events} = this.props;
    const selectedEvent = events.find(event => event.id === this.state.selectedEventId);
    return (
      <View
        style={styles.wrapper}>
        <Animated.View
          style={[
            styles.map,
            {transform: [{translateY: this.state.pan.interpolate({
              inputRange: this.inputRange,
              outputRange: this.outputRange
            })
          }]
         }]}>
          <MapView
            style={styles.map}
            initialRegion={{
              latitude: 47.910444,
              longitude: 33.391776,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            onPress={() => this.showEventInfo(false)}>
            {this.props.events.map(event => {
              return (
                <MapView.Marker
                  key={event.idEvent}
                  onPress={() => {
                    this.setState({
                      selectedEventId: event.id,
                    });
                    this.showEventInfo(true);
                  }}
                  style={{zIndex: event.participantsCount}}
                  coordinate={{
                    latitude: event.latitude,
                    longitude: event.longitude,
                  }}>
                  <EventMarker
                    selected={(this.state.selectedEventId === event.id) && this.state.showEventInfo}
                    event={event}/>
                </MapView.Marker>);
            })}
            {this.state.currentLocation &&
            <MapView.Marker
              coordinate={{
                latitude: this.state.currentLocation.latitude,
                longitude: this.state.currentLocation.longitude,
              }}>
              <UserMarker/>
            </MapView.Marker>}
          </MapView>
        </Animated.View>
        <Animated.View
          style={[
            {position: 'absolute', left: 0, bottom: 0, right: 0},
            {transform: [{translateY: this.state.pan}]}
            ]}
          {...this._panResponder.panHandlers}>
          <EventInfo loaded={this.state.selectedEventId !== null} event={selectedEvent}/>
        </Animated.View>
        <Animated.View style={{
          position: 'absolute', left: 0, bottom: 0, right: 0, height: FULL_INFO_HEIGHT,
          transform: [{translateY: Animated.add(this.state.pan, FULL_INFO_HEIGHT)}]
        }}>
          <EventFullInfo loaded={this.state.selectedEventId !== null} event={selectedEvent}/>
        </Animated.View>
      </View>
    );
  }
}

function mapStateToProps({events}) {
  return {events};
}

export default connect(mapStateToProps, {addEvent, getEvents})(EventsMap);