import React, { Component } from 'react';
import { FlatList, View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { utils } from '../../styles/global';
import Event from '../../components/events/list/Event';
import styles from '../../styles/pages/events/eventsList';

class Login extends Component {
  static navigationOptions = {
    title: 'Список'
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      events: [
        { image: 'http://mcselect.ru/wp-content/uploads/2014/08/aktivnyj-turizm.jpg', name: 'Велотур', date: '20 июня, 18:00', place: 'Кривой Рогу, ул. Пушкина, д. Колотушкина', participants: 7, friends: 2},
        { image: 'http://mcselect.ru/wp-content/uploads/2014/08/aktivnyj-turizm.jpg', name: 'Велотур', date: '20 июня, 18:00', place: 'Кривой Рогу, ул. Пушкина, д. Колотушкина', participants: 7, friends: 2},
      ]
    };
  }
  renderFooter = () => {
    if(this.state.loading) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  addNew = () => {
    if(this.state.loading) return;
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false,
        events: [...this.state.events,...[
          { image: 'http://mcselect.ru/wp-content/uploads/2014/08/aktivnyj-turizm.jpg', name: 'Велотур', date: '20 июня, 18:00', place: 'Кривой Рогу, ул. Пушкина, д. Колотушкина', participants: 7, friends: 2},
          { image: 'http://mcselect.ru/wp-content/uploads/2014/08/aktivnyj-turizm.jpg', name: 'Велотур', date: '20 июня, 18:00', place: 'Кривой Рогу, ул. Пушкина, д. Колотушкина', participants: 7, friends: 2},
          { image: 'http://mcselect.ru/wp-content/uploads/2014/08/aktivnyj-turizm.jpg', name: 'Велотур', date: '20 июня, 18:00', place: 'Кривой Рогу, ул. Пушкина, д. Колотушкина', participants: 7, friends: 2},
          { image: 'http://mcselect.ru/wp-content/uploads/2014/08/aktivnyj-turizm.jpg', name: 'Велотур', date: '20 июня, 18:00', place: 'Кривой Рогу, ул. Пушкина, д. Колотушкина', participants: 7, friends: 2},
        ]]
      });

    }, 2000);
  }
  render() {
    return(
      <FlatList
        style={styles.wrapper}
        data={this.state.events}
        keyExtractor={(item, index)=>index}
        renderItem={({item}) => <Event {...item} navigation={this.props.navigation} />}
        ListFooterComponent={this.renderFooter}
        onEndReached={this.addNew}
      />
    );
  }
}

export default connect(null, )(Login);