import React, { Component } from 'react';
import { ScrollView, View, Text, TextInput, DatePickerAndroid,
  TimePickerAndroid, TouchableOpacity, Alert, Image } from 'react-native';
import { connect } from 'react-redux';
import { utils } from '../../styles/global';
import styles from '../../styles/pages/events/eventForm';
import Button from '../../components/general/Button';
import MapView from 'react-native-maps';

const mapMarker = require('../../resources/images/map_marker.png');

class EventForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventName: '',
      description: '',
      showMap: false,
      date: new Date(),
      time: new Date(),
      latitude: 47.910444,
      longitude: 33.391776,
    };
  }
  static navigationOptions = {
    title: 'Добавить мероприятие'
  };
  showDatePicker = async () => {
    try {
      const options = { date: this.state.date };
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action !== DatePickerAndroid.dismissedAction) {
        let date = new Date(year, month, day);
        if(date < new Date(new Date().setHours(0,0,0,0))) {
          Alert.alert(
            'Неправильная дата',
            'Нельзя установить прошедшую дату',
            [
              {text: 'Ок'},
            ],
          )
        } else {
          this.setState({date});
        }
      }

    } catch ({code, message}) {
      console.warn(`Error in example: `, message);
    }
  };
  showTimePicker = async () => {
    try {
      const options = { hour: this.state.time.getHours(),
        minute: this.state.time.getMinutes(), is24Hour: true };
      const {action, hour, minute} = await TimePickerAndroid.open(options);
      if (action !== TimePickerAndroid.dismissedAction) {
        let time = new Date(this.state.date.setHours(hour, minute));
        if(time < new Date()) {
          Alert.alert(
            'Неправильное время',
            'Нельзя установить прошедшее время',
            [
              {text: 'Ок'},
            ],
            { cancelable: false }
          )
        } else {
          this.setState({ time });
        }
      }

    } catch ({code, message}) {
      console.warn(`Error in example': `, message);
    }
  };
  getDataInFormat(date) {
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if(dd<10){
      dd='0'+dd;
    }
    if(mm<10){
      mm='0'+mm;
    }
    return  dd+'/'+mm+'/'+yyyy;
  }
  getTimeInFormat(date) {
    let hh = date.getHours();
    let mm = date.getMinutes();
    let yyyy = date.getFullYear();
    if(hh<10){
      hh='0'+hh;
    }
    if(mm<10){
      mm='0'+mm;
    }
    return  hh+':'+mm;
  }
  onPressMapButton() {
    let latitude = this.state.latitude;
    let longitude = this.state.longitude;
    if(this.state.showMap) {
      latitude = this.mapview.__lastRegion.latitude;
      longitude = this.mapview.__lastRegion.longitude;
    }
    this.setState({ showMap: !this.state.showMap, latitude, longitude })
  }
  render() {
    return(
      <ScrollView style={styles.wrapper}>
        <View style={styles.row}>
          <Text style={styles.labelText}>
            {'Название:'}
          </Text>
          <TextInput
            style={styles.inputName}
            onChangeText={text => this.setState({ eventName: text })}
            value={this.state.eventName}
            underlineColorAndroid={'white'}
          />
        </View>
        <View style={styles.row}>
          <Text style={styles.labelText}>
            {'Описание:'}
          </Text>
          <TextInput
            style={styles.inputDescription}
            onChangeText={text => this.setState({ description: text })}
            value={this.state.description}
            multiline
            underlineColorAndroid={'white'}
          />
        </View>
        <View style={[styles.row, utils.flexDirectionRow]}>
          <Text style={styles.labelText}>
            {'Дата:'}
          </Text>
          <TouchableOpacity onPress={() => this.showDatePicker()}>
            <Text style={styles.labelText}>
              {this.getDataInFormat(this.state.date)}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.row, utils.flexDirectionRow]}>
          <Text style={styles.labelText}>
            {'Время:'}
          </Text>
          <TouchableOpacity onPress={() => this.showTimePicker()}>
            <Text style={styles.labelText}>
              {this.getTimeInFormat(this.state.time)}
            </Text>
          </TouchableOpacity>
        </View>
        <Button onPress={() => this.onPressMapButton()} style={styles.placeButton} text={'Указать местоположение'} />
        {this.state.showMap &&
        <View style={styles.mapWrapper}>
          <MapView
            style={styles.map}
            ref={(mapview) => { this.mapview = mapview; }}
            initialRegion={{
              latitude: this.state.latitude,
              longitude: this.state.longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          />
          <Image style={styles.mapMarker} source={mapMarker} />
        </View>}
        <Button text={'Сохранить'} />
      </ScrollView>
    );
  }
}


export default EventForm;