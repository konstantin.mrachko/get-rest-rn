import React, { Component } from 'react';
import { View, Text, Image, TextInput, Alert } from 'react-native';
import { connect } from 'react-redux';
import styles from '../../styles/pages/login';
import { utils } from '../../styles/global';
import Button from '../../components/general/Button';
import FacebookButton from '../../components/login/FacebookButton';
import GoogleButton from '../../components/login/GoogleButton';
import TextButton from '../../components/general/TextButton';
import login from '../../actions/authorization/login';
import loginFb from '../../actions/authorization/loginFb';

class Login extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }
  handleLoginError(error) {
    if (error.message === 'fb_login_cancelled') {
      Alert.alert(
        'Авторизация отменена',
        'А жаль :(',
        [
          { text: 'Ок' },
        ],
      );
    } else {
      Alert.alert(
        'Ошибка',
        error.toString(),
        [
          { text: 'Ок' },
        ],
      );
    }
  }
  handleLogin(loginPromise) {
    //todo start showing loader
    loginPromise.then(() => {
      //todo end showing loader
      this.props.navigation.navigate('coreApp');
    }).catch(this.handleLoginError);
    //todo end showing loader
  }
  onLoginPress() {
    this.handleLogin(this.props.login(this.state.email, this.state.password));
  }
  onFbLoginPress() {
    this.handleLogin(this.props.loginFb());
  }
  render() {
    return(
      <View style={styles.wrapper}>
        <View style={styles.loginWrapper}>
          <TextInput
            style={styles.loginInput}
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
            keyboardType="email-address"
            placeholder="Email"
          />
          <TextInput
            style={styles.loginInput}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            secureTextEntry
            placeholder="Пароль"
          />
          <Button text="Войти" buttonStyle={styles.loginButton} onPress={() => this.onLoginPress()}/>
          <View style={styles.socialWrapper}>
            <FacebookButton buttonStyle={styles.socialButton} onPress={() => this.onFbLoginPress()}/>
            <GoogleButton buttonStyle={styles.socialButton} />
          </View>
        </View>
        <View style={styles.registerWrapper}>
          <Text style={utils.textCenter}>Нет аккаунта?</Text>
          <TextButton
            text="Зарегистрироваться"
            buttonStyle={styles.registerButton}
            textStyle={styles.registerButtonText}
            onPress={() => this.props.navigation.navigate('register')}
          />
        </View>
      </View>
    );
  }
}

export default connect(null, {login, loginFb})(Login);