/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import React, { Component } from 'react';
import {AsyncStorage} from 'react-native';
import { Provider } from 'react-redux';
import AppNavigator from '../../navigators/AppNavigator';
import store from '../../utils/store';
import moment from 'moment';
import ruLocale from 'moment/locale/ru';

moment.locale('ru', ruLocale);

export default class Root extends Component {
  componentDidMount() {
    AsyncStorage.setItem('show-geo-dialog', 'true');
  }
  render() {
    return (
      <Provider store={store}>
        <AppNavigator/>
      </Provider>
    );
  }
}