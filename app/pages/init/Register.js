/**
 * Created by kostiantynmrachko on 5/22/17.
 */
import React, { Component } from 'react';
import { View, Text, Image, TextInput } from 'react-native';
import { connect } from 'react-redux';
import styles from '../../styles/pages/register';
import { utils } from '../../styles/global';
import Button from '../../components/general/Button';
import { emailValidation, passwordValidation, confirmValidation } from '../../utils/validation';
import registrationAction from '../../actions/authorization/registration';

class Register extends Component {
  static navigationOptions = {
    title: 'Регистрация'
  };
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
    };
  }
  onPress(){
    if (!this.isValid()) {
      return;
    }

    this.props.registrationAction(
      this.state.email,
      this.state.password
    );

    this.props.navigatiom.navigate('profileForm');
  }
  isValid() {
    return emailValidation(this.state.email) &&
      passwordValidation(this.state.password) &&
      confirmValidation(this.state.password, this.state.confirmPassword);
  }
  render() {
    return(
      <View style={styles.wrapper}>
        
        <View style={styles.loginWrapper}>
          <TextInput
            style={styles.registerInput}
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
            keyboardType="email-address"
            placeholder="Email"
            underlineColorAndroid="transparent"
          />
          <TextInput
            style={styles.registerInput}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            secureTextEntry
            placeholder="Пароль"
            underlineColorAndroid="transparent"
          />
          <TextInput
            style={styles.registerInput}
            onChangeText={(confirmPassword) => this.setState({confirmPassword})}
            value={this.state.confirmPassword}
            secureTextEntry
            placeholder="Повторите пароль"
            underlineColorAndroid="transparent"
          />
          <Button text="Вперед!" filled buttonStyle={styles.registerButton} onPress={()=>this.onPress()}/>
        </View>
      </View>
    );
  }
}

export default connect(null, {registrationAction})(Register);