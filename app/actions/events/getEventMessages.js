/**
 * Created by kostiantynmrachko on 8/26/17.
 */
import { SET_EVENT_MESSAGES } from '../../constants/actionTypes';
import moment from 'moment';

export default function (idEvent) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        dispatch({
          type: SET_EVENT_MESSAGES,
          payload: {
            idEvent,
            messages: [
              {
                idMessage: new Date().valueOf() + 23,
                idUserSender: 13,
                image_100: 'http://lorempixel.com/100/100/',
                name: 'Alex Dmytriv',
                message: 'Привет! Проверяю что наш чатец работает',
                timeOfSend: moment().subtract(7, 'days').unix()
              },
              {
                idMessage: new Date().valueOf() + 24,
                idUserSender: 19,
                image_100: 'http://lorempixel.com/100/100/',
                name: 'Julia V',
                message: 'Hey! Похоже что работает',
                timeOfSend: moment().subtract(6, 'days').unix()
              },
              {
                idMessage: new Date().valueOf() + 25,
                idUserSender: 18,
                image_100: 'http://lorempixel.com/100/100/',
                name: 'Den Domanitskyi',
                message: 'Выглядит как чат. Норм',
                timeOfSend: moment().subtract(2, 'days').unix()
              }
            ]
        }});
        resolve();
      }, 600);
    });
  };
}