/**
 * Created by kostiantynmrachko on 8/25/17.
 */
import { SET_EVENTS } from '../../constants/actionTypes';
import moment from 'moment';

export default function (longitude, latitude, radius = 100000) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        dispatch({
          type: SET_EVENTS,
          payload: [
            {
              idEvent: 1,
              latitude: 47.9104 + Math.random() / 10,
              longitude: 33.391776 + Math.random() / 10,
              eventName: "Велопоездка по Карачунам",
              fullDesc: 'Увидим много всего интересно. Буду рад компании',
              startTime: moment().add(3, 'days').unix(),
              author: { //todo add to the backend
                id: 12, name: 'Kostiantyn Mrachko', image_100: 'http://lorempixel.com/100/100/'
              },
              participants: [ //todo add this to the backend
                {id: 13, name: 'Alex Dmytriv', image_100: 'http://lorempixel.com/100/100/'},
                {id: 15, name: 'Kostya Gut', image_100: 'http://lorempixel.com/100/100/'},
                {id: 18, name: 'Den Domanitskyi', image_100: 'http://lorempixel.com/100/100/'}
              ]
            },
            {
              idEvent: 2,
              latitude: 47.9104 + Math.random() / 10,
              longitude: 33.391776 + Math.random() / 10,
              eventName: "Велотур по Вечернему",
              fullDesc: 'Увидим много всего интересно и не очень. Буду рад компании, но поеду и один',
              startTime: moment().add(2, 'days').unix(),
              author: {
                id: 19, name: 'Julia V', image_100: 'http://lorempixel.com/100/100/'
              },
              participants: [
                {id: 13, name: 'Alex Dmytriv', image_100: 'http://lorempixel.com/100/100/'},
                {id: 18, name: 'Den Domanitskyi', image_100: 'http://lorempixel.com/100/100/'}
              ]
            },
            {
              idEvent: 3,
              latitude: 47.9104 + Math.random() / 10,
              longitude: 33.391776 + Math.random() / 10,
              eventName: "Велотур по Вечернему 2",
              fullDesc: 'Увидим много всего неинтересного. Не буду рад компании, поеду один',
              startTime: moment().add(8, 'days').unix(),
              author: {id: 18, name: 'Den Domanitskyi', image_100: 'http://lorempixel.com/100/100/'},
              participants: [
                {id: 13, name: 'Alex Dmytriv', image_100: 'http://lorempixel.com/100/100/'},
                {id: 19, name: 'Julia V', image_100: 'http://lorempixel.com/100/100/'},
                {id: 12, name: 'Kostiantyn Mrachko', image_100: 'http://lorempixel.com/100/100/'}
              ]
            }
          ]
        });
        resolve();
      }, 600);
    });
  };
}