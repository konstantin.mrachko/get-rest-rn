/**
 * Created by kostiantynmrachko on 8/26/17.
 */
import { SET_EVENT_MESSAGE } from '../../constants/actionTypes';
import moment from 'moment';

export default function (idEvent, message) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        dispatch({
          type: SET_EVENT_MESSAGE,
          payload: {
            idEvent,
            message: {
              idMessage: new Date().valueOf(),
              idUserSender: getState().authentication.user.id,
              image_100: 'http://lorempixel.com/100/100/',
              name: getState().authentication.user.name,
              message,
              timeOfSend: moment().unix()
            }
          }
        });
        resolve();
      }, 600);
    });
  }
}