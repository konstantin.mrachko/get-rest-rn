/**
 * Created by kostiantynmrachko on 8/26/17.
 */
import { REMOVE_EVENT } from '../../constants/actionTypes';

export default function (idEvent) {
  return function (dispatch) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        dispatch({
          type: REMOVE_EVENT,
          payload: idEvent
        });
        resolve();
      }, 600);
    });
  };
}