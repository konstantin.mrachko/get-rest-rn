/**
 * Created by kostiantynmrachko on 5/29/17.
 */
import { SET_EVENT } from '../../constants/actionTypes';

export default function (eventName, fullDesc, startTime, latitude, longitude) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        dispatch({
          type: SET_EVENT,
          payload: {
            idEvent: new Date().valueOf(),
            latitude,
            longitude,
            eventName,
            fullDesc,
            startTime,
            author: getState().authentication.user,
            participants: []
          }
        });
        resolve();
      }, 600);
    });
  }
}