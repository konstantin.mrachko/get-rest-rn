/**
 * Created by kostiantynmrachko on 8/26/17.
 */
import { SET_EVENT } from '../../constants/actionTypes';
import moment from 'moment';

export default function (idEvent, visiting) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        const event = getState().events.find(event => event.idEvent === idEvent);
        let {participants} = event;
        participants = visiting ?
          [...participants, getState().authentication.user] :
          participants.filter(user => user.id !== getState().authentication.user.id);
        dispatch({
          type: SET_EVENT,
          payload: {...event, participants}
        });
        resolve();
      }, 600);
    });
  };
}