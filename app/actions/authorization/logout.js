/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import { LOGOUT } from '../../constants/actionTypes';

export default function () {
  return {
    type: LOGOUT
  };
}