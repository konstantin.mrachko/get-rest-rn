import axios from '../../utils/axios';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { getNetworkState } from '../../utils/networkState';
import { API_HOST_URL } from '../../constants/constants';
import { LOGIN } from '../../constants/actionTypes';

export default function authFacebookAction() {
  return function (dispatch, getState) {
    return getNetworkState().then(() =>
      LoginManager.logInWithReadPermissions(['public_profile', 'email', 'user_friends'])
    ).then(result =>
      result.isCancelled ?
        Promise.reject(new Error('fb_login_cancelled')) :
        AccessToken.getCurrentAccessToken()
    ).then(data => {
      console.log(data);
      const url = API_HOST_URL + 'loginBySocial';
      return Promise.resolve();//axios.post(url, {token: data.accessToken.toString()})
    }).then(response => {
      dispatch({
        type: LOGIN,
        payload: {
          token: 'test',
          userId: 99999
        }
      });
      return Promise.resolve();
    });
  }
}