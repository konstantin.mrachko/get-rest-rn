/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import { LOGIN, ADD_USER, ADD_EVENT } from '../../constants/actionTypes';

export default function (email, password) {
  return function (dispatch, getState) {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        dispatch({
          type: LOGIN,
          payload: {
            token: 'test',
            user: {
              id: 12,
              name: 'Kostiantyn Mrachko',
              image_100: 'http://lorempixel.com/100/100/',
              image_400: 'http://lorempixel.com/400/400/'
            }
          }
        });
        resolve();
      }, 300);
    });
  };
}