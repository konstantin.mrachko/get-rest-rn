/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import {combineReducers} from 'redux';
import loginReducer from './authentication';
import events from './events';
import eventsMessages from './eventsMessages';

export default combineReducers({
  authentication: loginReducer,
  events: events,
  eventsMessages: eventsMessages,
});