/**
 * Created by kostiantynmrachko on 8/26/17.
 */
import { SET_EVENT_MESSAGES, SET_EVENT_MESSAGE } from '../constants/actionTypes';

const initialState = [];

// export default function (state = initialState, action) {
//   switch (action.type) {
//     case SET_EVENT_MESSAGES: {
//       return [
//         ...state.filter(eventMessages => eventMessages.idEvent !== action.payload.idEvent),
//         action.payload
//       ];
//     }
//     case SET_EVENT_MESSAGE: {
//       return state.map(eventMessages =>
//           eventMessages.idEvent === action.payload.idEvent ?
//             {
//               ...eventMessages,
//               messages: [
//                 ...eventMessages.messages.filter(message => message.idMessage !== action.payload.message.idMessage),
//                 action.payload.message
//               ]
//             } :
//             eventMessages
//       );
//     }
//     default:
//       return state;
//   }
// }

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_EVENT_MESSAGES: {
      return {
        ...state,
        [action.payload.idEvent]: action.payload.messages
      };
    }
    case SET_EVENT_MESSAGE: {
      return {
        ...state,
        [action.payload.idEvent]: [
            ...state[action.payload.idEvent].filter(message => message.idMessage !== action.payload.message.idMessage),
            action.payload.message
          ]
      };
    }
    default:
      return state;
  }
}