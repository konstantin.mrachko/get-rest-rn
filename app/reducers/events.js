/**
 * Created by kostiantynmrachko on 8/25/17.
 */
import { SET_EVENTS, SET_EVENT, ADD_EVENTS, REMOVE_EVENT } from '../constants/actionTypes';
import unionBy from 'lodash/unionBy';

const initialState = [];

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_EVENTS: {
      return [
        ...action.payload
      ];
    }
    case ADD_EVENTS: {
      return unionBy(state, action.payload, event => event.idEvent);
    }
    case SET_EVENT: {
      return [
        ...state.filter(event => event.idEvent !== action.payload.idEvent),
        action.payload
      ];
    }
    case REMOVE_EVENT: {
      return state.filter(event => event.idEvent !== action.payload);
    }
    default:
      return state;
  }
}