/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import { LOGIN, LOGOUT } from '../constants/actionTypes';

const initialState = {
  token: null,
  userId: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        ...action.payload
      };
    }
    case LOGOUT: {
      return {
        initialState
      };
    }
    default:
      return state;
  }
}