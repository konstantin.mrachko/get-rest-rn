import { StyleSheet } from 'react-native';
import { colors } from '../../constants/uiConstants';

export default StyleSheet.create({
  button: {
    width: 140,
    paddingVertical: 8,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.accent,
    borderRadius: 10,
    marginHorizontal: 10,
    marginVertical: 15,
  },
  textButton: {
    fontSize: 20,
    fontWeight: '600',
  },
  pressedButtonText: {
    color: 'white'
  },
  pressedButton: {
    backgroundColor: colors.accent,
  },
});