import { StyleSheet } from 'react-native';
import { colors } from '../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
    backgroundColor: 'white',
    paddingVertical: 7,
    paddingHorizontal: 30,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: colors.grey,
  },
  img: {
    width: 90,
    height: 90,
  },
  text: {
    fontSize: 22,
    fontWeight: '500',
    flex: 1,
    marginLeft: 30,
  }
});