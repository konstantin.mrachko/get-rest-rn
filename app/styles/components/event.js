import { StyleSheet } from 'react-native';
import { colors } from '../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    height: 200,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginHorizontal: 15,
    marginBottom: 15,
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
  textTop: {
    alignSelf: 'flex-end',
  },
  textBig: {
    fontSize: 24,
    color: 'white',
    marginVertical: 7,
  },
  textSmall: {
    fontSize: 15,
    color: 'white',
  },
  buttonsWrapper: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 10,
    marginBottom: 10,
    borderBottomColor: colors.border,
    borderBottomWidth: 1.5,
  },
  button: {
    width: 140,
    paddingVertical: 8,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.accent,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  textButton: {
    fontSize: 20,
    fontWeight: '600',
  },
  pressedButtonText: {
    color: 'white'
  },
  pressedButton: {
    backgroundColor: colors.accent,
  }
});