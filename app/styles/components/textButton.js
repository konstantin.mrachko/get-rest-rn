/**
 * Created by kostiantynmrachko on 5/21/17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    borderWidth: 0,
    backgroundColor: 'transparent',
  }
});