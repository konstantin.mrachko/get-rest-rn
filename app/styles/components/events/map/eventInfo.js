/**
 * Created by kostiantynmrachko on 6/6/17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    height: 100,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  authorName: {
    fontSize: 18,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 50
  },
  textWrapper: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 24
  },
  participantsCount: {
    color: 'grey',
    fontSize: 14
  },
  button: {
    marginTop: 10
  }
});