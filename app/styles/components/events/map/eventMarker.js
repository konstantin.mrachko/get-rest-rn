/**
 * Created by kostiantynmrachko on 6/6/17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../../../../constants/uiConstants';

export default StyleSheet.create({
  markerWrapper: {
    width: 44,
    height: 54,
    alignItems: 'center',
    justifyContent: 'center',
  },
  markerText: {
    fontSize: 22,
    color: 'white',
    fontWeight: "bold",
    position: 'absolute',
    top: 6
  },
});