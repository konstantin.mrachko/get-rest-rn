import { StyleSheet } from 'react-native';
import { colors } from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 15,
    margin: 15,
    marginTop: 0,
    elevation: 2,
  },
  titleRow: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  name: {
    fontSize: 20,
    fontWeight: '600',
  },
  date: {
    fontSize: 18,
    color: colors.grey,
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 200,
    marginRight: 15,
  },
  text: {
    fontSize: 20,
    flex: 1,
  },
});