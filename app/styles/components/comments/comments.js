import { StyleSheet } from 'react-native';
import { colors } from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.greyLight,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    padding: 20,
    paddingTop: 30,
    backgroundColor: 'white',
    marginBottom: 15,
  },
  inputWrapper: {
    paddingHorizontal: 25,
    paddingVertical: 15,
    backgroundColor: 'white',
    margin: 15,
    marginTop: 0,
    elevation: 2,
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 200,
    marginRight: 15,
  },
  input: {
    fontSize: 20,
    flex: 1,
    textAlignVertical: 'top',
  },
  writing: {
    height: 120,
  },
  sendButton: {
    color: colors.accent,
    fontSize: 18,
    fontWeight: '600',
    alignSelf: 'flex-end'
  }
});