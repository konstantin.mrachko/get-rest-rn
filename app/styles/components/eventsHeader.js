/**
 * Created by kostiantynmrachko on 7/13/17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    height: 56,
    padding: 16,
    backgroundColor: 'white',
    justifyContent: 'space-between',
    elevation: 4
  },
  container: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, .9)',
    marginHorizontal: 16,
  },
  searchInput: {
    height: 42,
    flex: 1,
    fontSize: 18,
    color: 'rgba(0, 0, 0, .9)',
    marginHorizontal: 16,
    alignSelf: 'center'
  }
});