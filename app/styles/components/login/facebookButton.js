/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    borderColor: colors.facebookColor,
    backgroundColor: colors.facebookColor,
  },
});