/**
 * Created by kostiantynmrachko on 5/20/17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../../constants/uiConstants';

const borderRadius = 5;

export default StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
    padding: 10,
    backgroundColor: 'white',
    borderColor: colors.accent,
    borderWidth: 2,
    alignItems: 'center',
    borderRadius: borderRadius,
  },
  filledWrapper: {
    alignSelf: 'stretch',
    padding: 10,
    backgroundColor: colors.accent,
    borderColor: 'white',
    borderWidth: 2,
    alignItems: 'center',
    borderRadius: borderRadius,
  },
  text: {
    fontWeight: 'bold',
    color: colors.accent,
    fontSize: 22
  },
  filledText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 22
  }
});