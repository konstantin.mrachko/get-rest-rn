/**
 * Created by admin on 20.05.17.
 */
import { StyleSheet } from 'react-native';
import { colors } from '../constants/uiConstants';

export const utils = StyleSheet.create({
  textCenter: {
    textAlign: 'center',
  },
  flex1: {
    flex: 1,
  },
  flexDirectionRow: {
    flexDirection: 'row'
  },
  textBold: {
    fontWeight: 'bold',
  },
  marginBottom20: {
    marginBottom: 20,
  },
  regularButton: {
    width: 140,
    paddingVertical: 8,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.accent,
    borderRadius: 10,
    marginHorizontal: 10,
    marginVertical: 15,
  },
  regularButtonText: {
    fontSize: 20,
    fontWeight: '600',
  },
  backgroundColorWhite: {
    backgroundColor: 'white',
  },
  alignSelfStretch: {
    alignSelf: 'stretch'
  }
});