/**
 * Created by kostiantynmrachko on 5/23/17.
 */
import {StyleSheet} from 'react-native';
import {colors} from '../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: 30,
    backgroundColor: colors.accent,
    justifyContent: 'center'
  },
  registerInput: {
    fontSize: 22,
    marginTop: 20,
    borderRadius: 5,
    paddingHorizontal: 15,
    backgroundColor: 'white',
  },
  registerButton: {
    marginTop: 40
  }
});