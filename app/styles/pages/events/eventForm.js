import { StyleSheet } from 'react-native';
import { colors } from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  row: {
    alignSelf: 'stretch',
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  labelText: {
    fontSize: 20,
    marginRight: 20,
  },
  inputName: {
    fontSize: 20,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 15,
    marginTop: 10,
  },
  inputDescription: {
    fontSize: 20,
    height: 110,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 15,
    marginTop: 10,
    textAlignVertical: 'top',
  },
  placeButton: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 7,
    alignSelf: 'stretch',
    paddingVertical: 7,
  },
  mapWrapper: {
    alignSelf: 'stretch',
    height: 400,
    marginVertical: 20,
    marginHorizontal: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  },
  mapMarker: {
    width: 25,
    height: 55,
    resizeMode: 'contain',
    marginTop: 200 - 30,
    alignSelf: 'center',
  }
});