/**
 * Created by kostiantynmrachko on 5/23/17.
 */
import { StyleSheet } from 'react-native';
import {colors} from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  }
});