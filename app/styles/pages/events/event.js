import { StyleSheet } from 'react-native';
import { colors } from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  image: {
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    height: 200,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  header: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    paddingTop: 10,
  },
  dateWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  textDate: {
    fontSize: 50,
    fontWeight: '600',
  },
  textSmall: {
    fontSize: 14,
  },
  textBig: {
    fontSize: 24,
    marginTop: 7,
    fontWeight: '500',
  },
  text: {
    fontSize: 18,
  },
  descriptionWrapper: {
    flex: 3,
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  contentWrapper: {
    paddingHorizontal: 20,
    alignSelf: 'stretch',
    marginBottom: 20,
  },
  icon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginVertical: 5,
  },
  buttonsWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20,
  }
});