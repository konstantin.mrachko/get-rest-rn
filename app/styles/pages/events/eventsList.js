import { StyleSheet } from 'react-native';
import { colors } from '../../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    marginTop: 10,
    backgroundColor: 'white',
  },
});