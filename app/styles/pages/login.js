/**
 * Created by admin on 20.05.17.
 */
import {StyleSheet} from 'react-native';
import {colors} from '../../constants/uiConstants';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: 30,
    backgroundColor: 'white',
    justifyContent: 'flex-end'
  },
  loginWrapper: {
    paddingTop: 60,
    flex: 1,
    justifyContent: 'center'
  },
  loginButton: {
    marginTop: 20
  },
  loginInput: {
    fontSize: 22,
    marginTop: 20,
    borderColor: '#29A9EB'
  },
  socialWrapper: {
    marginTop: 30,
  },
  socialButton: {
    marginTop: 10
  },
  registerWrapper: {
    paddingTop: 40,
    flex: 0,
  },
  registerButton: {
    marginHorizontal: 30,
    padding: 5
  },
  registerButtonText: {
    fontSize: 16,
    fontWeight: 'normal'
  }
});