/**
 * Created by admin on 20.05.17.
 */
import {StyleSheet} from 'react-native';
import {colors} from '../../../constants/uiConstants';

export default StyleSheet.create({
  userImage: {
    height: 200,
    resizeMode: 'cover',
  },
  name: {
    alignSelf: 'center',
    fontSize: 22,
    fontWeight: '500',
    marginVertical: 25,
  },
  followersWrapper: {
    height: 100,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  followersText: {
    fontSize: 22,
  },
  followersCount: {
    fontSize: 26,
    fontWeight: '500',
  },

  mapWrapper: {
    alignSelf: 'stretch',
    height: 200,
    marginVertical: 20,
    marginHorizontal: 20,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  },
  events: {
    alignSelf: 'center',
    fontSize: 22,
  }
});