/**
 * Created by kostiantynmrachko on 8/25/17.
 */
import { createStore, applyMiddleware } from 'redux';
import {AsyncStorage} from 'react-native';
import rootReducer from '../reducers/root';
import thunk from 'redux-thunk';
import axios from './axios';
import {LOGIN} from '../constants/actionTypes';

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

let token = null;
AsyncStorage.getItem('sabrify-auth')
  .then(function (authData) {
    if (authData) {
      authData = JSON.parse(authData);
      if (authData.token !== null) {
        store.dispatch({type: LOGIN, payload: authData});
        axios.defaults.headers['Authorization'] = authData.token;
      }
      //todo add splash screen that hides here
    }
  });

store.subscribe(function () {
  const newToken = store.getState().authentication.token;
  if (token !== newToken) {
    token = newToken;
    if (token) {
      axios.defaults.headers['Authorization'] = token;
    } else {
      delete axios.defaults.headers['Authorization'];
    }
    AsyncStorage.setItem('sabrify-auth', JSON.stringify(store.getState().authentication));
  }
});

export default store;