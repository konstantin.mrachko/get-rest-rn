import { NetInfo, Alert } from 'react-native'

/**
 * check network connection
 */
class NetworkState {
  checkConnection(onConnected) {
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected) {
        onConnected();
      } else {
        Alert.alert("Отсутствует интернет", 'Для работы приложения необходима связь с интернетом', [{
          text: 'Попробовать снова',
          onPress: () => { this.checkConnection(onConnected); }
        }]);
      }
    });
  }
}

export function getNetworkState() {
  return new Promise((resolve, reject) => {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        resolve();
      } else {
        Alert.alert("Отсутствует интернет", 'Для работы приложения необходима связь с интернетом', [{
          text: 'Попробовать снова',
          onPress: () => {
            getNetworkState()
              .then(() => resolve())
          }
        }]);
      }
    });
  });
}

//export default new NetworkState();
