import errorMessage from './errorMessage';

export function emailValidation(email) {
  if (email === null || email === undefined || email === '') {
    errorMessage("Некоррректный емейл");
    return false;
  }
  const regularExpression = /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
  const result = regularExpression.test(email);
  if (result) {
    return true;
  }
  errorMessage("Некоррректный емейл");
  return false;
}

export function passwordValidation(password) {
  const result = password.length >= 6;
  if (result) {
    return true;
  }
  errorMessage("Некоррректный пароль");
  return false;
}

export function confirmValidation(password1, password2) {
  const result = password1 === password2;
  if (result) {
    return true;
  } else {
    errorMessage("Ошибка при подтверждении пароля");
    return false;
  }
}