/**
 * Created by kostiantynmrachko on 8/25/17.
 */
import axios from 'axios';
import {API_HOST} from '../constants/constants';

const axiosInstance = axios.create({
  baseURL: API_HOST,
  responseType: 'json'
});

// export const interceptors = axiosInstance.interceptors.response.use(
//   response => response, error => {
//     const {response} = error;
//     if (response && (response.status === 401)) {
//       history.push('/auth/login');
//     }
//     return Promise.reject(error);
//   });

export default axiosInstance;