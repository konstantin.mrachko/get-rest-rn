import { Alert } from 'react-native';

export default function errorMessage(text) {
  Alert.alert(
    'Ошибка ввода',
    text,
    [
      { text: 'OK' },
    ],
  );
}
