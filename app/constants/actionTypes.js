/**
 * Created by kostiantynmrachko on 5/20/17.
 */
/* login */
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

/* users */
export const ADD_USER = 'ADD_USER';

/* events */
export const SET_EVENTS = 'SET_EVENTS';
export const SET_EVENT = 'SET_EVENT';
export const ADD_EVENTS = 'ADD_EVENTS';
export const REMOVE_EVENT = 'REMOVE_EVENT';
export const SET_EVENT_MESSAGES = 'SET_EVENT_MESSAGES';
export const SET_EVENT_MESSAGE = 'SET_EVENT_MESSAGE';
