export const ANY = 'any';
export const INTEGER = 'integer';
export const FLOAT = 'float';
export const STRING = 'string';
export const DATE = 'date';
export const BIRTHDATE = 'birthdate';
