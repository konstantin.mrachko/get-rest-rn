/**
 * Created by admin on 20.05.17.
 */
export const colors = {
  accent: '#29A9EB',
  facebookColor: '#3B5998',
  googleColor: '#DB4437',
  marker: '#f4511e',
  border: 'grey',
  greyLight: 'rgb(230,230,230)',
  grey: 'grey',
};